package com.zuitt.example;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Activity {
    public static void main(String[] args){
        // [ACTIVITY SOLUTION]
        HashMap<String, Integer> games = new HashMap<>();

        games.put("Mario Odyssey", 50);
        games.put("Super Smash Bros. Ultimate", 20);
        games.put("Luigi's Mansion", 15);
        games.put("Pokemon Sword", 30);
        games.put("Pokemon Shield", 100);

        games.forEach((key, value) -> {
            System.out.println(key + " has " + value + " stocks left.");
        });

        ArrayList<String> topGames = new ArrayList<>();

        games.forEach((key, value) -> {
            if(value <= 30){
                topGames.add(key);
                System.out.println(key + " has been added to the top games list!");
            }
        });

        System.out.println("Our shop's top games:");
        System.out.println(topGames);

        //[STRETCH GOAL]
        Scanner userInput = new Scanner(System.in);
        boolean addItem = true;

        while(addItem){
            System.out.println("Would you like to add an Item? Yes or No?");
            String ans = userInput.nextLine();

            if(ans.equalsIgnoreCase("Yes")){
                System.out.println("Add Item Name");
                String itemName = userInput.nextLine();
                System.out.println("Add Number of Stocks");
                int itemStock = userInput.nextInt();

                games.put(itemName, itemStock);
                System.out.println(itemName + " has been added with " + itemStock + " stocks.");

                addItem = false;

            } else {
                addItem = false;
                System.out.println("Thank you!");
            }
        }

    }
}
