package com.zuitt.example;

import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args){
    /*
         Exception Handling
         -> Handles runtime errors so that the normal flow of the application can be maintained. It is an event that disrupts the normal flow of the program.
    */
        Scanner input = new Scanner(System.in);
        int num = 0;
        System.out.println("Input a number:");

        try{
            num = input.nextInt();
        } catch(Exception e){
            System.out.println("Invalid input");
            e.printStackTrace();
        }
        System.out.println("The number you entered is " + num);

        // other example:
        try{
            int divideByZero = 5 / 0;
            System.out.println("Rest of code in try block");
        } catch(ArithmeticException e){
            // can print e to show the specific name of the exception
            System.out.println(e);
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
}
